#include <ctime>
#include <iostream>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <math.h>
using namespace std;
#include "Arreglo.h"

string Arreglos::catch_string(string in){
	//Esta funcion recibe un string ingresado por teclado en la funcion principal//
	//para validar la entrada de un int primero se crea y define is_true como verdadero por default//
	bool is_true = true;
	//Se abre un ciclo while que corre mientras is_true sea verdadero//
	while (is_true == true){
		//Dentro del while se abre un ciclo for que analiza el string recibido//
		for(int j = 0; j < in.size(); j++){
			//Se recorre y se comparan los valores ASCII contenidos en el string//
			//A los valores ASCII de los numros del 0-9 y al valor de '-'//
			if((in[j] >= 48 && in[j] <= 57) || in[j] == 45){
				//si el contenido del string no coincide con un valor ASCII numerico o de "-" is_true se iguala a falso//
				is_true = false;
			}else{
				//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
				is_true = true;
				break;
			}
		}
		//Luego si se encuentra algo que no es un numero o '-' en el string ingresado//
		if(is_true == true){
			//Se entrega un mensaje de error//
			cout<<"Por favor ingrese valores numéricos:"<<endl;
			//Y se pide otro ingreso//
			getline(cin, in);
		}
	}
	//De esta forma se asegura que la funcion 'stoi()' no entregue error//
	return in;
}

int Arreglos::in_quant(){
	int quant;
	string in;
	cout<<"Ingrese cantidad de numeros a ordenar:"<<endl;
	cout<<"(1-1.000.000)"<<endl;
	getline(cin, in);
	in = catch_string(in);
	quant = stoi(in);
	if((quant > 1000000) && (quant <= 0)){
		quant = in_quant();
	}
	return quant;	
}
void Arreglos::print_a(int arr[], int cant){
	for(int i = 0; i < cant; i++){
		cout<<"[a]"<<arr[i]<<" ";
	}
	cout<<endl;
}

void Arreglos::fill(int arr[], int quant){
	int num;
	for (int i = 0; i < quant; i++){
		num = (rand() % 10000) +1;
		arr[i] = num;
	}
}

void Arreglos::copy(int arr[], int sel[], int quant){
	for(int i = 0; i < quant; i++){
		sel[i] = arr[i];
	}
}

void Arreglos::print_arrs(int quant, int sel[], int qui[]){
	cout<<"Seleccion: ";
	for(int i = 0; i < quant; i++){
		cout<<"[a]"<<sel[i]<<" ";
	}
	cout<<"\n"<<"Quicksort:";
	for(int j = 0; j < quant; j++){
		cout<<"[a]"<<qui[j]<<" ";
	}
	cout<<endl;
}

void Arreglos::selec(int cant, int arr[]){
	int i, menor, k, j;
	
	for(i = 0; i <= (cant - 2); i++){
		menor = arr[i];
		k = i;
		
		for(j = (i+1); j <= (cant - 1); j++){
			if(arr[j] < menor){
				menor = arr[j];
				k = j;
			}
		}
		arr[k] = arr[i];
		arr[i] = menor;
	}
}

void Arreglos::quick(int cant, int arr[]){
	int tope, ini, fin, pos;
	int pilamenor[100];
	int pilamayor[100];
	int izq, der, aux;
	bool band;
	
	tope = 0;
	pilamenor[tope] = 0;
	pilamayor[tope] = cant-1;
	
	while (tope >= 0) {
		ini = pilamenor[tope];
		fin = pilamayor[tope];
		tope = tope - 1;
		
		// reduce
		izq = ini;
		der = fin;
		pos = ini;
		band = true;
		
		while (band == true) {
			while ((arr[pos] <= arr[der]) && (pos != der)){
				der = der - 1;
			}
			if (pos == der) {
				band = false;
			} else {
				aux = arr[pos];
				arr[pos] = arr[der];
				arr[der] = aux;
				pos = der;
		
				while ((arr[pos] >= arr[izq]) && (pos != izq)){
					izq = izq + 1;
				}
				if (pos == izq) {
					band = false;
				} else {
					aux = arr[pos];
					arr[pos] = arr[izq];
					arr[izq] = aux;
					pos = izq;
				}
			}
		}
		
		if (ini < (pos - 1)) {
			tope = tope + 1;
			pilamenor[tope] = ini;
			pilamayor[tope] = pos - 1;
		}
		
		if (fin > (pos + 1)) {
			tope = tope + 1;
			pilamenor[tope] = pos + 1;
			pilamayor[tope] = fin;
		}
	}
}
