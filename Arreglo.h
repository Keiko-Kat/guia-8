#include <ctime>
#include <iostream>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <math.h>
using namespace std;

class Arreglos{
	public:
		int in_quant();
		string catch_string(string in);
		void print_a(int arr[], int cant);
		void selec(int cant, int arr[]);
		void quick(int cant, int arr[]);
		void print_arrs(int quant, int sel[], int qui[]);
		void fill(int arr[], int quant);
		void copy(int arr[], int sel[], int quant);
};
