#include <ctime>
#include <iostream>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <math.h>
using namespace std;
#include "Arreglo.h"

int main(int argc, char *argv[]){
	char *opt = argv[1];
	
	if(argc < 2){
		//si los parametros entregados por terminal son menos que los necesarios
	    cout<<"||Faltan parámetros||"<<endl;
	    //se envia un mensaje de error y se cierra el programa
	    return -1;
	}
    int opi = static_cast<int>(opt[0]);
    if(opi != 110 && opi != 115){
		cout<<"||Ingreso incorrecto||"<<endl;
		
		return -1;
	}
		
	clock_t time = 0;
    tm time_now;
    int time_rand = time_now.tm_isdst;
    srand(time_rand);
	
	int quant, selec, quic;
	Arreglos a = Arreglos();
	
	quant = a.in_quant();
	int arr[quant], sel[quant], qui[quant];
	
	a.fill(arr, quant);
	a.copy(arr, sel, quant);
	a.copy(arr, qui, quant);
	
	if(opi == 115){
		a.print_a(arr, quant);
	}
	
	time = clock();
	a.selec(quant, sel);
	selec = clock() - time;
	
	time = clock();
	a.quick(quant, qui);
	quic = clock() - time;
	
	cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
 	cout<<"Metodo              || tiempo "<<endl;
	cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
	cout<<"Selección           || "<< selec << " milisegundos"<<endl;
	cout<<"Quicksort           || "<<  quic << " milisegundos"<<endl;
	cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
	
	if(opi == 115){
		a.print_arrs(quant, sel, qui);
	}
	
	return 0;
}
