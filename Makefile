prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = Ejer_8.cpp Arreglo.cpp
OBJ = Ejer_8.o Arreglo.o
APP = guia_8

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

