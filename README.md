# guia-8


EL ejercicio fue resuelto en Geany, ejecutado desde la terminal para testeo en linux 4.9.0-12-amd64


Ejecutar make

Ejecutar la orden "./guia_8 n" o "./guia_8 s"

La opcion "n", solo entrega el tiempo que tarda cada algoritmo en organizar el arreglo
y la opcion "s" muestra los resultados de cada arreglo una vez ordenados.

Si se le entrega un parametro incorrecto al inicio del programa, o si se le entregan menos parametros de los 
necesarios, el programa muestra un error y se cierra.

La cantidad de numeros que contiene el arreglo se le pide al usuario una vez que el programa este corriendo.


